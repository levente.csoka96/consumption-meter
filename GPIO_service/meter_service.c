/*
*       Author: Csoka Levente
*       All rights reserved.
*/

#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <time.h>

int main(int argc, char *argv[])
{
        int fd;
        int log;
        char value;
        struct pollfd poll_gpio;

        poll_gpio.events = POLLPRI;

        // According to linux documentatino:
        //      1. export pin 6
        //      2. set direction to in
        //      3. set edge detection falling
        //      4. set a poll for the value
        
        log = open("/etc/meter_log.log", O_WRONLY | O_APPEND | O_CREAT);
        fd = open("/sys/class/gpio/export", O_WRONLY);
        write(fd, "6", 1);
        close(fd);
        fd = open("/sys/class/gpio/gpio6/direction", O_WRONLY);
        write(fd, "in", 2);
        close(fd);
        fd = open("/sys/class/gpio/gpio6/edge", O_WRONLY);
        write(fd, "falling", 7);
        close(fd);
        fd = open("/sys/class/gpio/gpio6/value", O_RDONLY);
        poll_gpio.fd = fd;

        poll(&poll_gpio, 1, -1);
        read(fd, &value, 1);

        while(1)
        {
                poll(&poll_gpio, 1, -1);
                if((poll_gpio.revents & POLLPRI) == POLLPRI)
                {
                        int prev_value = 0;

                        lseek(fd, 0, SEEK_SET);
                        read(fd, &value, 1);
                        usleep(10000);

                        char buff[22];
                        struct tm *sTm;

                        time_t now = time(0);
                        sTm = localtime(&now);
                        strftime(buff, sizeof(buff), "%Y-%m-%d %H:%M:%S\n", sTm);
                        write(log, &buff, sizeof(buff));
                }
        }
        close(fd);
        return EXIT_SUCCESS;
}
