# Fogyasztásmérő

---

## Controller GUI

![alt text](https://gitlab.com/levente.csoka96/consumption-meter/-/raw/master/doc/vezerlo.png "Vezerlo")

### Tartalom
- `ConsumptionMeterController.exe`
- `libEGL.dll`
- `Qt5Core.dll`
- `Qt5Gui.dll`
- `Qt5Widgets.dll`
- `platforms`

### Használat
Indítás a `ConsumptionMeterController.exe` indításával. A futáshoz szükséges összes függőség a csomag tartalma.

1. Első lépésben az IP címet kitöltve tudunk ez eszközhöz csatlakozni.
2. Második felületen beállíthatjuk az eszköz tulajdonságait
3. Az **Alkalmaz** gomb lenyomásával tudjuk feltölteni a kitöltött adatokat az eszközre, illetve a  **Frissítés** gombbal lekérni.

> **Megjegyzés:** IP cím vagy Netmask beállítása után az eszköz újraindul, viszont ez az alkalmazás nem, ezért érdemes kézzel újraindítani.

## Handler

### Tartalom
- `handler_service.py`
- `handler_service.service`

### Használat
Ez a modul előre telepített, és automatikusan indul a rendszerrel együtt. A *222*-es porton keresztül kommunikálhatunk vele a GUI segítségével.

Ezen modul segítségével végezhetünk beállításokat az eszközben, illetve felelős az `.xlsx` file kitöltéséért, felmásolásáért.

## Service

### Tartalom
- `meter_service.o`
- `gpio_service.service`

### Használat
Ez a modul szintén előre telepített és a rendszerrel együtt indul. Külső kommunikációra nincs lehetőség, a kivezetések rövidrezárását felügyeli és logolja.
> **Megjegyzés:** Ugyan nincs meghatározott felsőhatára a rövidrezárás frekvenciájának, sem alsóhatára az időtartamának, lehetőség szerint maradjon az *1Khz* alatt.
