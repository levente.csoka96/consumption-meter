#include "ConnectionHandler.h"
#include <qmessagebox.h>


std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}


void ConnectionHandler::run()
{
	char buffer[128];
	//----------------------- TICK ---------------------
	while(true)
	{
		std::string message = "tick";
		for (int i = message.length(); i < 128; i++)
		{
			message.append(" ");
		}
		send(this->server, message.c_str(), 128, 0);
		recv(this->server, buffer, sizeof(buffer), 0);
		tick(QString(buffer).toInt());
		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
	
}


void ConnectionHandler::initConnection(QString ip)
{

	WSAStartup(MAKEWORD(2, 0), &wsa_data);
	server = socket(AF_INET, SOCK_STREAM, 0);

	InetPton(AF_INET, s2ws(ip.toUtf8().constData()).c_str(), &addr.sin_addr.s_addr);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(222);

	::connect(server, reinterpret_cast<SOCKADDR *>(&addr), sizeof(addr));
}

void ConnectionHandler::sendData(QString name, QString server, QString ip, QString netmask)
{
	//----------------------- DEVICE NAME ---------------------
	std::string message = "set DeviceName ";
	message.append(name.toUtf8().constData());
	for (int i = message.length(); i < 128; i++)
	{
		message.append(" ");
	}
	send(this->server, message.c_str(), 128, 0);

	//-------------------- SERVER ADDRESS ---------------------
	message = "set Server ";
	message.append(server.toUtf8().constData());
	for (int i = message.length(); i < 128; i++)
	{
		message.append(" ");
	}
	send(this->server, message.c_str(), 128, 0);

	//------------------- NETWORK SETTINGS --------------------
	message = "ip ";
	message.append(ip.toUtf8().constData());
	message.append(" ");
	message.append(netmask.toUtf8().constData());
	for (int i = message.length(); i < 128; i++)
	{
		message.append(" ");
	}
	//send(this->server, message.c_str(), 128, 0);

	//--------------------- REBOOT -----------------------------
	message = "restart";
	for (int i = message.length(); i < 128; i++)
	{
		message.append(" ");
	}
	send(this->server, message.c_str(), 128, 0);
	
	QMessageBox messageBox;
	messageBox.information(0, "Sikeres beallitas!", "Minden adatot sikerult beallitani.\n Az eszkoz most ujraindul!");
	messageBox.setFixedSize(500, 200);

	/*
	closesocket(this->server);
	WSACleanup();*/
}

void ConnectionHandler::getData(QString* name, QString* server, QString* ip, QString* netmask)
{
	char buffer[128];
	//----------------------- DEVICE NAME ---------------------
	std::string message = "get DeviceName";
	for (int i = message.length(); i < 128; i++)
	{
		message.append(" ");
	}
	send(this->server, message.c_str(), 128, 0);
	recv(this->server, buffer, sizeof(buffer), 0);
	*name = QString(buffer);

	//-------------------- SERVER ADDRESS ---------------------
	message = "get Server ";
	for (int i = message.length(); i < 128; i++)
	{
		message.append(" ");
	}
	send(this->server, message.c_str(), 128, 0);
	recv(this->server, buffer, sizeof(buffer), 0);
	*server = QString(buffer);

	//------------------- NETWORK SETTINGS -------------------
	/*std::string message = "ip ";
	message.append(ip.toUtf8().constData());
	message.append(" ");
	message.append(netmask.toUtf8().constData());
	for (int i = message.length(); i < 128; i++)
	{
		message.append(" ");
	}
	send(this->server, message.c_str(), 128, 0);*/

}
