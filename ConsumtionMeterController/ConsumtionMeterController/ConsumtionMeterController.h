#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ConsumtionMeterController.h"
#include "ConnectionHandler.h"

class ConsumtionMeterController : public QMainWindow
{
    Q_OBJECT

public:
    ConsumtionMeterController(QWidget *parent = Q_NULLPTR);
	~ConsumtionMeterController();
	ConnectionHandler* connectionThread;

private:
    Ui::ConsumtionMeterControllerClass ui;

public slots:
	void on_apply_clicked();
	void on_refresh_clicked();
	void startThis();
	void updateTick(int t);

signals:
	void sendData(QString name, QString server, QString ip, QString netmask);
	void getData(QString* name, QString* server, QString* ip, QString* netmask);
	
};
