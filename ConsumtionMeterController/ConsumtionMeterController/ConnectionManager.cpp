#include "ConnectionManager.h"

ConnectionManager::ConnectionManager(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

void ConnectionManager::on_connectButton_clicked()
{
	initConnection(ui.ipLineEdit->text());
	startNext();
	connectionThread->start();
	this->close();
}