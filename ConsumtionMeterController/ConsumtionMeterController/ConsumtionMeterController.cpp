#include "ConsumtionMeterController.h"

ConsumtionMeterController::ConsumtionMeterController(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
}

void ConsumtionMeterController::on_apply_clicked()
{
	QString name = ui.deviceName->text();
	QString server = ui.serverAddress->text();
	QString ip = ui.ipAddressEdit->text();
	QString netmask = ui.netmask->text();
	sendData(name, server, ip, netmask);
}

void ConsumtionMeterController::on_refresh_clicked()
{
	QString name = "";
	QString server = "";
	QString ip = "";
	QString netmask = "";
	getData(&name, &server, &ip, &netmask);
	ui.deviceName->setText(name);
	ui.serverAddress->setText(server);
}

void ConsumtionMeterController::startThis()
{
	this->show();
}

void ConsumtionMeterController::updateTick(int t)
{
	ui.impulses->setText(QString::number(t));
}

ConsumtionMeterController::~ConsumtionMeterController()
{
	connectionThread->terminate();
}




