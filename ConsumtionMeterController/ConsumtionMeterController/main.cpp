#include "ConsumtionMeterController.h"
#include <QtWidgets/QApplication>
#include "ConnectionManager.h"
#include "ConnectionHandler.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	ConnectionHandler ch;
	ConnectionManager cm;
	ConsumtionMeterController w;

	cm.connectionThread = &ch;
	w.connectionThread = &ch;

	QObject::connect(&w, &ConsumtionMeterController::sendData, &ch, &ConnectionHandler::sendData);
	QObject::connect(&w, &ConsumtionMeterController::getData, &ch, &ConnectionHandler::getData);
	QObject::connect(&cm, &ConnectionManager::startNext, &w, &ConsumtionMeterController::startThis);
	QObject::connect(&cm, &ConnectionManager::initConnection, &ch, &ConnectionHandler::initConnection);
	QObject::connect(&ch, &ConnectionHandler::tick, &w, &ConsumtionMeterController::updateTick);

	//ch.start();
	cm.show();
	return a.exec();
}
