#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ConnectionManager.h"
#include "ConnectionHandler.h"

class ConnectionManager : public QMainWindow
{
	Q_OBJECT

public:
	ConnectionManager(QWidget *parent = Q_NULLPTR);
	ConnectionHandler* connectionThread;

private:
	Ui::ConnectionManager ui;

public slots:
	void on_connectButton_clicked();

signals:
	void initConnection(QString ip);
	void startNext();
};
