#pragma once
#include <QThread>
#include <winsock2.h>
#include <stdio.h>
#include <ws2tcpip.h>
#include <string.h>

#pragma comment(lib,"ws2_32.lib")

class ConnectionHandler : public QThread
{
	Q_OBJECT
public:
	void run();
public slots:
	void initConnection(QString ip);
	void sendData(QString name, QString server, QString ip, QString netmask);
	void getData(QString* name, QString* server, QString* ip, QString* netmask);

signals:
	void disconnected();
	void tick(int t);
private:
	WSADATA wsa_data;
	SOCKADDR_IN addr;
	SOCKET server;
};
