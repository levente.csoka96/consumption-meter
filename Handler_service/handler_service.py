import socket
import sys
import fileinput
import configparser
import threading
from datetime import datetime,timedelta
import time
import os
from openpyxl import Workbook, load_workbook
from subprocess import run, PIPE


def replace_config(_key : str, value):
    for line in fileinput.input("/etc/meter_provider.ini", inplace=True):
        if _key in line:
            print("{} = {}\n".format(_key,value), end='')
        else:
            print(line, end='')

def get_config(_key : str):
    config = configparser.ConfigParser()
    config.read("/etc/meter_provider.ini")
    result = "Invalid"
    try:
        result = config["CONFIG"][_key]
    finally:
        pass
    return result

def get_impulses( base_time = datetime.now()):
    counter = 0
    time_period = get_config("UploadInterval")
    with open("/etc/meter_log.log", "rb") as file:
        for line in file.readlines():
            date_in_string = line.decode("utf-8", errors="ignore")[-20:]
            if(len(date_in_string.strip()) >= 19):
                dt = datetime.strptime(date_in_string.strip(), "%Y-%m-%d %H:%M:%S")
                elapsed = datetime.now() - dt
                if elapsed.total_seconds() < int(time_period)*60:
                    counter+=1
    return counter

def apply_network_settings(ip : str, netmask : str):
    with open("/etc/network/interfaces", "w") as f:
        f.write("source /etc/network/interfaces.d/*\n")
        f.write("allow-hotplug eth0\n")
        f.write("auto eth0\n")
        f.write("iface eth0 inet static\n")
        f.write("\taddress {}\n".format(ip))
        f.write("\tnetmask {}\n".format(netmask))
        f.write("auto lo\n")
        f.write("iface lo inet loopback\n")

def backup_service():
    while(1):
        dt_start = datetime.now()
        minutes_to_wait = 15 - dt_start.minute%15 if dt_start.minute%15>0 else 0
        seconds_to_wait = 60-dt_start.second
        print("waiting for: {}:{}".format(minutes_to_wait, seconds_to_wait))
        time.sleep(minutes_to_wait*60+seconds_to_wait)
        # do the data collection and file sync
        ticks_to_write = get_impulses()
        server = get_config("Server")
        file_name = get_config("DeviceName")+".xlsx"

        # copy the file to tmp regardless of its origin
        if(server.startswith("ftp")):
            pass # for now
        else:
            try:
                run(["smbclient",server,"N"], stdout=PIPE, input="get "+file_name+" /tmp/"+file_name,encoding='ascii')
            except:
                print("[ERROR]: File could not be fetched!")

        # update the local file
        workbook = None
        try:
            workbook = load_workbook("/tmp/"+file_name)
        except:
            workbook = Workbook()
        sheet = workbook.active
        index = 1
        while sheet.cell(row=index, column=1).value :
            index += 1
        sheet.cell(row=index, column=1).value = (dt_start-timedelta(minutes=15)).strftime("%Y/%m/%d, %H:%M")
        sheet.cell(row=index, column=2).value = datetime.now().strftime("%Y/%m/%d, %H:%M")
        sheet.cell(row=index, column=3).value = "{}".format(ticks_to_write)
        workbook.save(filename="/tmp/"+file_name)

        # copy back the temporary file
        if(server.startswith("ftp")):
            pass # for now
        else:
            try:
                run(["smbclient",server,"N"], stdout=PIPE, input="put /tmp/"+file_name+" "+file_name,encoding='ascii')
            except:
                print("[ERROR]: File could not be loaded up!")

        time.sleep(60)


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('0.0.0.0', 222)
print('starting up on {} port {}'.format(*server_address))
sock.bind(server_address)
sock.listen(1)

daemon_thread = threading.Thread(target=backup_service, daemon=True)
daemon_thread.start()

while True:
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)
        bye = False
        while not bye:
            data = connection.recv(128)
            data = data.decode('utf-8')
            if data.startswith("bye"):
                bye = True
            if data.startswith("set"):
                replace_config(data.split(" ")[1].strip(), data.split(" ")[2].strip())
            if data.startswith("get"):
                connection.sendall("{}\0".format(get_config(data.split(" ")[1])).encode())
            if data.startswith("tick"):
                connection.sendall("{}\0".format(get_impulses()).encode())
            if data.startswith("ip"):
                new_ip = data.split(" ")[1]
                new_netmask = data.split(" ")[2]
                apply_network_settings(new_ip, new_netmask)
                connection.sendall("{} {}".format(new_ip, new_netmask).encode())
            if data.startswith("restart"):
                os.system("sudo rm *.xlsx")
                os.system("sudo reboot")
    except:
        pass
    finally:
        # Clean up the connection
        print("Connection closed!")
        connection.close()

